//
// Created by Apopis on 28.12.2017.
//

#ifndef LEHOTGIS_EDGEHASH_H
#define LEHOTGIS_EDGEHASH_H

#include <functional>

namespace std
{
    template<> struct hash<pair<size_t,size_t>>
    {
        typedef pair<size_t,size_t> argument_type;

        size_t operator()(argument_type const& arg) const noexcept
        {
            size_t const h1(hash<size_t>{}(arg.first));
            size_t const h2(hash<size_t>{}(arg.second));
            return h1 ^ (h2 << 1);
        }
    };
}

#endif //LEHOTGIS_EDGEHASH_H
