//
// Created by Apopis on 29.12.2017.
//

#ifndef LEHOTGIS_MAKEEDGEGRAPH_H
#define LEHOTGIS_MAKEEDGEGRAPH_H

#include "Graph.h"

class MakeEdgeGraph {
    using Edge = std::pair<size_t,size_t>;

    bool isAdjacent(const Edge&, const Edge&);
public:
    Graph operator()(const Graph&);
};

#endif //LEHOTGIS_MAKEEDGEGRAPH_H
