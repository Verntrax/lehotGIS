//
// Created by Apopis on 04.01.2018.
//

#ifndef LEHOTGIS_ENCHANCEDQUEUE_H
#define LEHOTGIS_ENCHANCEDQUEUE_H

#include <queue>
#include <unordered_set>

template <class T>
class EnhancedQueue {
    std::queue<T> queue;
    std::unordered_set<T> set;

public:
    EnhancedQueue() = default;
    EnhancedQueue(const EnhancedQueue&) = default;
    EnhancedQueue(EnhancedQueue&&) noexcept = default;

    EnhancedQueue& operator=(const EnhancedQueue&) = default;
    EnhancedQueue& operator=(EnhancedQueue&&) noexcept = default;

    bool empty() const {
        return set.size() == 0;
    }

    size_t size() const {
        return set.size();
    }

    T& pop() {
        while (!queue.empty()) {
            if (set.count(queue.front()) != 0) {
                T &result = queue.front();
                queue.pop();
                set.erase(result);
                return result;
            }
            queue.pop();
        }
        throw std::out_of_range("No elements in queue");
    }

    void push(const T &element) {
        set.insert(element);
        queue.push(element);
    }

    void erase(const T &element) {
        set.erase(element);
    }

    bool contains(const T &element) {
        return set.count(element) != 0;
    }
};

#endif //LEHOTGIS_ENCHANCEDQUEUE_H
