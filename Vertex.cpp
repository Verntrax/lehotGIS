//
// Created by Apopis on 28.12.2017.
//

#include "Vertex.h"
#include <stdexcept>
#include <string>

using namespace std;

Vertex::Vertex()
        : adjacent(), firstName(0), secondName(0), nameLevel(0)
{}

Vertex::Vertex(const size_t &firstName, const size_t &secondName)
        : adjacent(), nameLevel(0)
{
    addName(firstName);
    addName(secondName);
}

void Vertex::addAdjacent(const size_t &other) {
    adjacent.insert(other);
}

 const unordered_set<size_t>& Vertex::getAdjacent() const {
    return adjacent;
}

void Vertex::addName(const size_t &name) const {
    switch (nameLevel) {
        case 0:
            firstName = name;
            break;
        case 1:
            if (name == firstName)
                throw out_of_range("Name already possessed");
            else if (name > firstName)
                secondName = name;
            else {
                secondName = firstName;
                firstName = name;
            }
            break;
        default:
            throw out_of_range("Vertex fully named");
    }
    ++nameLevel;
}

size_t Vertex::getFirstName() const {
    if (nameLevel >= 1)
        return firstName;
    throw out_of_range("Vertex does not have any name");
}

size_t Vertex::getSecondName() const {
    if (nameLevel == 2)
        return secondName;
    throw out_of_range("Vertex does not have second name");
}

bool Vertex::hasName(const size_t &name) const {
    switch(nameLevel){
        case 1:
            return name == firstName;
        case 2:
            return (name == firstName || name == secondName);
        default:
            return false;
    }
//    return (name == firstName || name == secondName);
}
std::string Vertex::parseName() const { // funkcja pomocnicza do wyswietlania nazwy wierzcholka w konwencji podanej przez P. G. H. Lehota
    std::string name;
    try{
        name += to_string(this->getFirstName());
    }
    catch(const std::out_of_range& e){
        name += "?";
    }
    name += " - ";
    try{
        name += to_string(this->getSecondName());
    }
    catch(const std::out_of_range& e){
        name += "?";
    }
    return name;
}

bool Vertex::isFullyNamed() const {
    return nameLevel==2;
}
