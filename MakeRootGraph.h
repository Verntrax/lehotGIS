//
// Created by Apopis on 29.12.2017.
//

#ifndef LEHOTGIS_MAKEROOTGRAPH_H
#define LEHOTGIS_MAKEROOTGRAPH_H

#include "Graph.h"

struct MakeRootGraph {
    Graph operator()(const Graph&);
};

#endif //LEHOTGIS_MAKEROOTGRAPH_H
