//
// Created by Apopis on 29.12.2017.
//

#include "MakeRootGraph.h"

Graph MakeRootGraph::operator()(const Graph &edgeGraph) {
    Graph graph;

    for (const auto &vertex : edgeGraph.getVertices()) {
        if (vertex.getSecondName() + 1 > graph.getNoVertices()) {
            for (size_t i = graph.getNoVertices(); i <= vertex.getSecondName(); ++i)
                graph.addVertex();
        }
        graph.addEdge(vertex.getFirstName(),vertex.getSecondName());
    }
    return graph;
}