//
// Created by Apopis on 29.12.2017.
//

#include "MakeEdgeGraph.h"

bool MakeEdgeGraph::isAdjacent(const Edge &first, const Edge &second) {
    if (first.first == second.first)
        return true;
    if (first.second == second.first)
        return true;
    if (first.first == second.second)
        return true;
    return first.second == second.second;
}

Graph MakeEdgeGraph::operator()(const Graph &graph) {
    Graph edgeGraph;
    const auto &edges = graph.getEdges();

    for (const auto &edge : edges)
        edgeGraph.addVertex(edge.first,edge.second);

    for (size_t i = 0; i < edgeGraph.getNoVertices(); ++i) {
        for (size_t j = i + 1; j < edgeGraph.getNoVertices(); ++j) {
            if (isAdjacent(edges[i],edges[j]))
                edgeGraph.addEdge(i,j);
        }
    }
    return edgeGraph;
}
