//
// Created by Apopis on 04.01.2018.
//

#include <iostream>
#include "Lehot.h"

using namespace std;

Lehot::Lehot()
        : graph(nullptr), halfNamed(), basic1(nullptr), basic2(nullptr), basicAdj(), result()
{}

Graph Lehot::operator()(const Graph &edgeGraph) {
    graph = &edgeGraph;
    cout<<"Najpierw sprawdzmy, czy zadany graf nie jest trojkatem: "<<endl;
    if (!graphIsValid()) { //sprawdzamy, czy graf jest trojkatem, jesli tak to ma dwa rozwiazania - my podamy jedno z nich
        cout << "\tGraf jest trojkatem, istnieja dwa grafy pierwotne. Zwracam jeden z mozliwych - trojkat." << endl;
        cout << "\t0: 0-1\n" <<"\t1: 0-2\n"<<"\t2: 1-2\n";
        cout << "KONIEC PROGRAMU" << endl;
        result = edgeGraph; //trojkat moze byc swoim grafem krawedziowym
        return result;
    }
    cout<<"\tGraf nie jest trojkatem \nPrzejdzmy do Kroku 1\n" << endl;
    step1(); //jesli zadany graf nie jest trojkatem to przechodzimy do kroku 1 algorytmu
    return result;
}

void Lehot::step1() {
    cout << "------------------------- KROK 1 ----------------------"<<endl;
    cout << "Wybierzmy dwa dowolne wierzcholki bazowe, od ktorych zaczniemy analize:";
    cout << " 0, "; //w tym przypadku akurat zaczynamy zawsze od wierzcholka oznaczonego 0

    basic1 = &graph->getVertices()[0]; //basic1 jest wybranym wierzcholkiem zadanego grafu
    const size_t& basic2Index = *basic1->getAdjacent().begin();
    basic2 = &graph->getVertices()[basic2Index]; // basic2 jest jednym z (pierwszym) wierzchołków sąsiadujących z basic1

    cout << basic2Index <<endl;

    basic1->addName(1); //dodajemy wierzchołek basic1 do kliki 1
    cout << "Dodajemy wierzcholek 0 do kliki 1"<<endl;
    basic2->addName(1); //dodajemy wierzchołek basic2 do kliki 1
    cout << "Dodajemy wierzcholek "<< basic2Index <<" do kliki 1"<<endl;


    halfNamed.push(0); //dodajemy wierzchołek basic1 do listy półnazwanych wierzchołków - halfNamed - (zgodnie z konwencją Lehota)

    cout << "Dodajemy wierzcholek 0 do listy na wpół nazwanych wierzcholkow (halfNamed)"<<endl;
    cout << "\t 0: " << basic1->parseName() << endl;

    halfNamed.push(basic2Index); //basic2 tez nalezy do kliki 1, zatem takze musimy go dodac do listy halfNamed

    cout << "Dodajemy wierzcholek "<< basic2Index <<" do listy halfNamed"<<endl;
    cout << "\t " << basic2Index << ": "<< basic2->parseName() << endl;

    cout<<"Przejdzmy do Kroku 2\n" << endl;
    step2(); //przchodzimy do kroku 2 algorytmu Lehota
}

void Lehot::step2() {
    cout << "------------------------- KROK 2 ----------------------"<<endl;
    cout <<"Tworzymy zbior X wierzcholkow sasiadujacych z oboma wierzcholkami bazowymi jednoczesnie:"<<endl;
    std::string output = "\tX = {";
    for (const size_t &adj : basic1->getAdjacent()) { //dla kazdego wierzchołka sąsiadującego z basic1
        if (basic2->getAdjacent().count(adj) != 0) {  //dla kazdego wierzchołka sąsiadującego z basic1 && z basic2
            output += to_string(adj); output += ", ";
            basicAdj.push_back(adj); //dodajemy go do naszej roboczej listy sąsiednich wierzchołków, które dalej będą analizowane
        }
    }
    output[output.size()-2] = '}';
    cout << output << endl;
    cout << "Rozmiar X to:\t" << basicAdj.size() <<", zatem:"<<endl;

    switch (basicAdj.size()) { //sprawdzamy ile jest takich wierzchołków sąsiadujących zarówno z basic1, jak i z basic2
        case 0: //jesli 0 to przejdzmy prosto do kroku 6
            cout<<"Przejdzmy do Kroku 6\n" << endl;
            step6();
            break;
        case 1: //jesli 1 to przejdzmy do kroku 3
            cout<<"Przejdzmy do Kroku 3\n" << endl;
            step3();
            break;
        case 2: //jesli 2 to przejdzmy do kroku 2
            cout<<"Przejdzmy do Kroku 4\n" << endl;
            step4();
            break;
        default: //jesli 3 i wiecej to idziemy do kroku 5
            cout<<"Przejdzmy do Kroku 5\n" << endl;
            step5();
    }
}

void Lehot::step3() {
    cout << "------------------------- KROK 3 ----------------------"<<endl;
    const size_t xIndex = basicAdj.front(); //wskaźnik na wierzchołek x - jedyny sąsiadujący z każdym wierzcholkiem bazowym jednoczesnie
    cout << "Wierzcholek "<< xIndex << " jest jedynym wierzcholkiem sasiadujacym z oboma wierzcholkami bazowymi" << endl;
    const Vertex &x = graph->getVertices()[xIndex];
    auto checkFun = [](unsigned int arg) { // argument przekazywany jako warunek funkcji sprawdzajacej ilosc sasiadow trojkata
        return arg == 1;
    };
    if (checkTriangleNeighbours(xIndex,0,*basic1->getAdjacent().begin(),checkFun)) // jesli istnieje taki wierzcholek sasiadujacy z dokładnie jednym wierzchołkiem trojkata {x, basic1, basic2}
    {
        cout << "Istnieje taki wierzcholek, ktory sasiaduje dokladnie z jednym wierzcholkiem trojkata {" << xIndex
             << ", " << 0 << ", " << *basic1->getAdjacent().begin() << "}" << endl;
        cout << "Przejdzmy do Kroku 6\n" << endl;
        step6();
    }
    else {
        cout << "Nie istnieje taki wierzcholek, ktory sasiaduje tylko z jednym wierzcholkiem trojkata {" << xIndex << ", " << 0 << ", " << *basic1->getAdjacent().begin() << "}" << endl;
        x.addName(0); // jeśli nie to x jest wierzcholkiem krzyzowym (dodajemy do kliki 0 oraz 2)
        x.addName(2);
        cout << "Znalezlismy wierzcholek krzyzowy:\n" << "\t" << xIndex << ": " << x.parseName() << endl;
        basicAdj.pop_front(); // usuwamy ze zbioru X
        cout << "Przejdzmy do Kroku 7\n" << endl;
        step7();
    }
}

void Lehot::step4() {
    cout << "------------------------- KROK 4 ----------------------"<<endl;
    auto it = basicAdj.begin();
    cout << "Wybieramy ze zbioru X w dowolny sposób wierzchołki x oraz y" << endl;
    const size_t &xIndex = *it;
    const size_t &yIndex = *(++it);

    cout << "Na przyklad:\n\t"<<"x = "<< xIndex <<endl << "\ty = "<< yIndex<<endl;

    const Vertex &x = graph->getVertices()[xIndex];
    const Vertex &y = graph->getVertices()[yIndex];


    if (x.getAdjacent().count(yIndex) == 0) {
        if (isOdd(xIndex,0,*basic1->getAdjacent().begin())) {
            y.addName(0);
            y.addName(2);
            cout << "Znalezlismy wierzcholek krzyzowy:\n" << "\t" << yIndex << ": " << y.parseName() << endl;
            basicAdj.erase(it);
        } else {
            x.addName(0);
            x.addName(2);
            cout << "Znalezlismy wierzcholek krzyzowy:\n" << "\t" << xIndex << ": " << x.parseName() << endl;
            basicAdj.pop_front();
        }
    } else
        cout<< "Wierzcholki x oraz y sasiaduja ze soba, zatem:" << endl;
    cout<<"Przejdzmy do Kroku 6\n" << endl;
    step6();
}

void Lehot::step5() {
    cout << "------------------------- KROK 5 ----------------------"<<endl;
    auto it = basicAdj.begin();

    const size_t &bIndex = *(it++);
    const size_t &aIndex = *(it++);

    const Vertex &b = graph->getVertices()[bIndex];
    const Vertex &a = graph->getVertices()[aIndex];

    cout << "Sa przynajmniej 3 wierzcholki, ktore sasiaduja z naszymi wierzcholkami bazowymi" << endl;
    cout << "Sposrod zbioru X wybierzmy dowolny wierzcholek b:\t" << bIndex << endl;
    cout << "Sposrod zbioru X wybierzmy dowolny wierzcholek a:\t" << aIndex << endl;
    cout << "\tCzy wierzcholek a sasiaduje z wierzchokiem b?" << endl;
    if (a.getAdjacent().count(bIndex) != 0) {
        cout << "\t\tSasiaduje, zatem sprawdzmy, czy istnieje taki z pozostalych wierzcholkow ze zbioru X, ktory nie sasiaduje z wierzcholkiem b = "<< bIndex << endl;
        while (it != basicAdj.end()) {
            const Vertex &c = graph->getVertices()[*it];
            if (c.getAdjacent().count(bIndex) == 0) {
                cout << "\t\t\tIstnieje, wierzcholek "<< *it <<", ktory takze sasiaduje z "<< bIndex <<endl;
                c.addName(0);
                c.addName(2);
                cout << "\t\t\tZnalezlismy wierzcholek krzyzowy:\n" << "\t" << *it << ": " << c.parseName() << endl;
                cout << "\t\t\tUsunmy wierzcholek " << *it << " z naszego roboczego zbioru X" << endl;
                basicAdj.erase(it);
                break;
            }
            ++it;
        }
    } else {
        const Vertex &c = graph->getVertices()[*it];
        cout << "\t\tNie sasiaduje, zatem sprawdzmy, czy wierzcholek a = " << aIndex << " sasiaduje z dowolnym trzecim wierzcholkiem, np.: "<< *it << endl;
        if (c.getAdjacent().count(aIndex) != 0) {
            cout << "\t\t\tSasiaduje, zatem ";
            b.addName(0);
            b.addName(2);
            cout << "znalezlismy wierzcholek krzyzowy - b:\n" << "\t\t\t\t" << bIndex << ": " << b.parseName() << endl;
            cout << "\t\t\tUsunmy wierzcholek " << bIndex << " z naszego roboczego zbioru X" << endl;
            basicAdj.pop_front();
        } else {
            cout << "\t\t\tNie sasiaduje, zatem ";
            a.addName(0);
            a.addName(2);
            cout << "znalezlismy wierzcholek krzyzowy - a:\n" << "\t\t\t\t" << aIndex << ": " << a.parseName() << endl;
            cout << "\t\t\tUsunmy wierzcholek " << aIndex << " z naszego roboczego zbioru X" << endl;
            basicAdj.erase(--it);
        }
    }
    cout << "Przejdzmy do Kroku 6\n" << endl;
    step6();
}

void Lehot::step6() {
    cout << "------------------------- KROK 6 ----------------------"<<endl;
    cout << "Kazdy z wierzcholkow w zbiorze X:\n * dodajemy do kliki: 1 \n * dodajemy do listy halfNamed, zatem:" << endl;
    for (const size_t &adj : basicAdj) {
        const Vertex &vertex = graph->getVertices()[adj];
        vertex.addName(1);
        halfNamed.push(adj);
        cout<< "\t"<<adj<<": " << vertex.parseName() << endl;
    }
    cout<<"Przejdzmy do Kroku 7\n" << endl;
    step7();
}

void Lehot::step7() {
    cout << "------------------------- KROK 7 ----------------------"<<endl;
    cout << "\n\t Na tym etapie wszystkie wierzcholki jednej z klik - 1 - sa juz w polowie nazwane i klika jest okreslona." << endl;
    cout << "Od tej pory bedziemy je wykorzystywac do nazwania tych jeszcze nienazwanych wierzcholkow. W tym celu: " << endl;
    cout<<"\nPrzejdzmy do Kroku 8\n" << endl;
    step8();
}

void Lehot::step8() { //w tym kroku zostanie nazwana reszta wierzcholkow
    cout << "------------------------- KROK 8 ----------------------"<<endl;
    size_t count = 0;
    cout << "Dzialamy w petli az wszystkie wierzcholki zostana w pelni nazwane:" << endl;
    cout << "\nRozpoczynamy proces nazywania wierzcholkow od wybrania dowolnego wierzcholka z listy halfNamed." <<endl;
    while (!halfNamed.empty()) {
        const size_t &nIndex = halfNamed.pop();
        cout << "Sprawdzamy wszystkie wierzcholki sasiadujace z wierzcholkiem "<< nIndex << endl;
        const Vertex &n = graph->getVertices()[nIndex];
        const size_t &clique = n.getFirstName();
        //cout << " (klika "<< clique << "):"<<endl;
        cout << "Probujemy nadac imie: "<< count <<endl;

        for (const size_t &kIndex : n.getAdjacent()) {
            const Vertex &k = graph->getVertices()[kIndex];
            cout<< "\t"<< kIndex << ":\tObecnie " << k.parseName();

            if (k.hasName(clique)){
                cout << "\n\t\t* zostal juz nazwany jednym z imion rownym - " << clique;
            }
            if (k.hasName(count)){
                cout << "\n\t\t* zostal juz nazwany jednym z imion - "<< count;
            }
            if (k.isFullyNamed()){
                cout << "\n\t\t* jest juz w pelni nazwany";
            }
            if (k.hasName(clique) || k.hasName(count) || k.isFullyNamed()) {
                cout << "\n\t\tZatem nie mozemy go nazwac w tej iteracji." << endl;
            }


            if (!k.hasName(clique) && !k.hasName(count) && !k.isFullyNamed()) {
                k.addName(count);
                cout << "\n\t\tNie jest jeszcze w pelni nazwany oraz nie posiada zadnego z imion "<< count <<", zatem zostaje nim nazwany: "<< endl;
                cout <<"\t\t"<< kIndex << ": " << k.parseName()<<endl;
                if (halfNamed.contains(kIndex)) {
                    cout << "\t\tWierzcholek " << kIndex << " jest juz w pelni nazwany, zatem usunmy go z listy halfNamed." << endl;
                    halfNamed.erase(kIndex);
                }
                else{
                    cout << "\t\tLista halfNamed nie zawierala wierzcholka " << kIndex << " zatem dodajmy go do niej, bo jeszcze nie jest w pelni nazwany."
                         << endl;
                    halfNamed.push(kIndex);
                }
            }
        }
        n.addName(count);
        cout <<"\t"<< nIndex << ": " << n.parseName()<<endl;
        ++count;
        if (count == 1)
            ++count;
    }
    cout << "Wszystkie wierzcholki zostaly w pelni nazwane." << endl;
    cout << "Sprawdzmy czy zadany graf jest grafem krawedziowym, w tym celu:" << endl;
    cout<<"\nPrzejdzmy do Kroku 9\n" << endl;
    step9();
}

void Lehot::step9() {
    cout << "------------------------- KROK 9 ----------------------"<<endl;
    char error[] = "Zadany graf nie jest grafem krawedziowym";//"Given graph is not an edge graph";

    cout<<"Sprawdzmy, czy graf krawedziowy otrzymanego przez nas grafu odpowiada grafowi wejsciowemu:" << endl;
    result = MakeRootGraph{}(*graph);
    Graph edgeGraph = MakeEdgeGraph{}(result);

    if (graph->getNoVertices() == edgeGraph.getNoVertices()) {
        for (size_t i = 0; i < graph->getNoVertices(); ++i) {
            const Vertex &vertex1 = graph->getVertices()[i];
            const Vertex &vertex2 = edgeGraph.getVertices()[i];

            if (vertex1.getAdjacent() != vertex2.getAdjacent())
                throw out_of_range(error);
        }
    } else
        throw out_of_range(error);
    cout << "Zadany graf jest grafem krawedziowym." << endl;
    //cout << "Wierzcholek zadanego grafu krawedziowego: wierzcholek1 grafu oryginalnego - wierzcholek2 grafu oryginalnego" << endl;
    for(size_t i=0; i< this->graph->getNoVertices(); i++)
    {
        cout << "\t" << i << ": " << graph->getVertices()[i].parseName() << endl;
    }
    cout << "KONIEC PROGRAMU" << endl;
}

bool Lehot::graphIsValid() const {
    return (graph->getNoVertices() != 3 || graph->getEdges().size() != 3);
}

bool Lehot::checkTriangleNeighbours(const size_t &vertex1Index, const size_t &vertex2Index, const size_t &vertex3Index,
                                    std::function<bool(unsigned int)> checkFun) const {
    const Vertex vertex1 = graph->getVertices()[vertex1Index];
    const Vertex vertex2 = graph->getVertices()[vertex2Index];
    const Vertex vertex3 = graph->getVertices()[vertex3Index];

    unordered_set<size_t> allAdj;
    //taking all unique vertices adj to triangle
    for (const size_t &adj : vertex1.getAdjacent())
        allAdj.insert(adj);
    for (const size_t &adj : vertex2.getAdjacent())
        allAdj.insert(adj);
    for (const size_t &adj : vertex3.getAdjacent())
        allAdj.insert(adj);

    for (const size_t &adj : allAdj) {
        if (adj != vertex1Index && adj != vertex2Index && adj != vertex3Index) {
            unsigned int neighbours = 0;
            if (vertex1.getAdjacent().count(adj) != 0)
                ++neighbours;
            if (vertex2.getAdjacent().count(adj) != 0)
                ++neighbours;
            if (vertex3.getAdjacent().count(adj) != 0)
                ++neighbours;
            if (checkFun(neighbours))
                return true;
        }
    }
    return false;
}

bool Lehot::isOdd(const size_t &vertex1Index, const size_t &vertex2Index, const size_t &vertex3Index) const {
    auto checkFun = [](unsigned int arg) {
        return arg == 1 || arg == 3;
    };
    return checkTriangleNeighbours(vertex1Index,vertex2Index,vertex3Index,checkFun);
}

