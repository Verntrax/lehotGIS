//
// Created by Apopis on 28.12.2017.
//

#include <algorithm>
#include "Graph.h"

using namespace std;

Graph::Graph(const size_t &noVertices)
        : vertices(noVertices)
{}

void Graph::addVertex() {//dodaje wierzcholek do grafu
    vertices.emplace_back();
}

void Graph::addVertex(const size_t &firstName, const size_t &secondName) {
    vertices.emplace_back(firstName,secondName);
}

void Graph::addEdge(const size_t &firstVertex, const size_t &secondVertex) {
    Edge edge = minmax(firstVertex,secondVertex);
    edges.push_back(edge);
    vertices[firstVertex].addAdjacent(secondVertex);
    vertices[secondVertex].addAdjacent(firstVertex);
}

const vector<Graph::Edge>& Graph::getEdges() const {
    return edges;
}

const vector<Vertex>& Graph::getVertices() const {
    return vertices;
}

size_t Graph::getNoVertices() const {
    return vertices.size();
}
