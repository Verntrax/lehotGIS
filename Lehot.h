//
// Created by Apopis on 04.01.2018.
//

#ifndef LEHOTGIS_LEHOT_H
#define LEHOTGIS_LEHOT_H

#include <list>
#include <functional>
#include "Graph.h"
#include "MakeRootGraph.h"
#include "MakeEdgeGraph.h"
#include "EnhancedQueue.h"

class Lehot {
    const Graph *graph;

    EnhancedQueue<size_t> halfNamed;

    const Vertex *basic1;
    const Vertex *basic2;
    std::list<size_t> basicAdj;

    Graph result;

    void step1();
    void step2();
    void step3();
    void step4();
    void step5();
    void step6();
    void step7();
    void step8();
    void step9();

    bool checkTriangleNeighbours(const size_t&, const size_t&,
                                 const size_t&, std::function<bool(unsigned int)>) const;
    bool isOdd(const size_t&, const size_t&, const size_t&) const;
    bool graphIsValid() const;

public:
    Lehot();
    Lehot(const Lehot&) = delete;
    Lehot(Lehot&&) = delete;

    Lehot& operator=(const Lehot&) = delete;
    Lehot& operator=(Lehot&&) = delete;

    Graph operator()(const Graph&);
};

#endif //LEHOTGIS_LEHOT_H
