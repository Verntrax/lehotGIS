#include <iostream>
#include "Lehot.h"

using namespace std;

int main() {


    //odczyt grafu z pliku
    //przypadki testowe pokazane w sprawozdaniu znajduja sie w folderze /lehotGIS/cmake-build-debug jako pliki tekstowe example*.txt
    size_t n;
    cin >> n;
    Graph graph(n);

    size_t vertex1;
    size_t vertex2;

    while (cin >> vertex1 >> vertex2)
        graph.addEdge(vertex1,vertex2);


    try             //jesli wyjatek zostanie wyrzucony to oznacza, ze graf wejsciowy nie jest grafem krawedziowym
    {
        Graph rootGraph = Lehot{}(graph);
    }
    catch(const std::out_of_range& e){
        cout << "Graf nie jest grafem krawedziowym!" << endl;
    }
    return 0;
}