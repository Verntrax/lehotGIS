//
// Created by Apopis on 28.12.2017.
//

#ifndef LEHOTGIS_VERTEX_H
#define LEHOTGIS_VERTEX_H

#include <unordered_set>

class Vertex {
    std::unordered_set<size_t> adjacent;
    mutable size_t firstName;
    mutable size_t secondName;
    mutable unsigned int nameLevel;

public:


    Vertex();
    Vertex(const Vertex&) = default;
    Vertex(Vertex&&) = default;
    Vertex(const size_t&, const size_t&);

    Vertex& operator=(const Vertex&) = default;
    Vertex& operator=(Vertex&&) = default;

    void addAdjacent(const size_t&);
    const std::unordered_set<size_t>& getAdjacent() const;
    void addName(const size_t&) const;
    size_t getFirstName() const;
    size_t getSecondName() const;
    bool hasName(const size_t&) const;
    std::string parseName() const;
    bool isFullyNamed() const;
};

#endif //LEHOTGIS_VERTEX_H
