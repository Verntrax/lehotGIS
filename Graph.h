//
// Created by Apopis on 28.12.2017.
//

#ifndef LEHOTGIS_GRAPH_H
#define LEHOTGIS_GRAPH_H

#include <vector>
#include "EdgeHash.h"
#include "Vertex.h"

class Graph {
    using Edge = std::pair<size_t, size_t>;

    std::vector<Vertex> vertices;
    std::vector<Edge> edges;

public:
    Graph() = default;
    explicit Graph(const size_t&);
    Graph(const Graph&) = default;
    Graph(Graph&&) = default;

    Graph& operator=(const Graph&) = default;
    Graph& operator=(Graph&&) = default;

    void addVertex();
    void addVertex(const size_t&, const size_t&);
    void addEdge(const size_t&, const size_t&);
    const std::vector<Edge>& getEdges() const;
    const std::vector<Vertex>& getVertices() const;
    size_t getNoVertices() const;
};

#endif //LEHOTGIS_GRAPH_H
